package com.geektechnique.configurationproperties.congifproperties;

import org.apache.catalina.core.ApplicationContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@EnableConfigurationProperties
public class CongifPropertiesApplication {

	public static void main(String[] args) {

		ConfigurableApplicationContext ctx = SpringApplication.run(CongifPropertiesApplication.class, args);
		MyAppConfig configuration = (MyAppConfig) ctx.getBean("myAppConfig");
		System.out.println(configuration.toString());
	}

}
